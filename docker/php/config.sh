#!/bin/bash -e

ENABLE_XDEBUG="${ENABLE_XDEBUG:='false'}"

if [ "${ENABLE_XDEBUG}" == "true" ]; then
docker-php-ext-enable xdebug
cat >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini <<CONFIG
[xdebug]
xdebug.profiler_enable=1
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_mode=req
xdebug.remote_port=9001
xdebug.remote_autostart=1
xdebug.remote_connect_back=1
xdebug.idekey=PHPSTORM
CONFIG
else
rm -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
fi

if [ -f /var/run/php-fpm.pid ] && [ -d /proc/$(</var/run/php-fpm.pid) ]; then
  kill -USR2 $(</var/run/php-fpm.pid) || true
fi

exec "$@"
