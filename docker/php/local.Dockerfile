FROM registry.gitlab.com/denkarusev/forum/php:latest

# install and enable xdebug
RUN pecl install xdebug-2.9.7 \
	&& docker-php-ext-enable xdebug

COPY config.sh /config.sh
ENTRYPOINT [ "/config.sh" ]

COPY start.sh /start.sh
CMD [ "/start.sh" ]
