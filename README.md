## <a name="requirements"></a> Системные требования

* Ubuntu 20.04.2 LTS
* make
* [Docker ≥ 20.10.0](https://docs.docker.com/engine/install/)
* [Docker-compose ≥ 1.28.0](https://docs.docker.com/compose/install/)

## <a name="install"></a> Установка

Инсталляция проходит разово выполнением действий:

1. Создаем файл с настройками приложения:
    ```bash
    make env
    ```
   создаст `.env`, который в случае необходимости следует отредактировать.

2. Выполняем установку и сборку проекта (требуется `sudo`):
    ```bash
    make install
    make build
    make up
    make prepare-app
    ```
3. Создаем тестовую базу данных (выполняется 1 раз):
    ```bash
    make create-test-db
    make restart
    ```

## <a name="up"></a> Запуск

Если "[Установка](#install)" прошла корректно, то запускаем командой:

```bash
make up
```

## <a name="up"></a> Другие команды

Справка:

```bash
make help
```

Остановка проекта:

```bash
make down
```

Перезапуск проекта:

```bash
make restart
```

Запуск тестов:

```bash
make test
```

Создание администратора (для доступа в админку):

```bash
make create-admin
```

## <a name="errors"></a> Возможные ошибки

Не удается найти APP_KEY

Решение:

```bash
make restart
```

## <a name="main_packages"></a> Основные пакеты

[Laravel Passport](https://github.com/laravel/passport)

[Lighthouse GraphQL Passport Auth](https://github.com/joselfonseca/lighthouse-graphql-passport-auth)

API: [Lighthouse GraphQL](https://github.com/nuwave/lighthouse)

Панель администратора: [Orchid - Laravel Admin Panel](https://github.com/orchidsoftware/platform)

## <a name="other_settings"></a> Дополнительная информация

Для авторизации используется пакет [Laravel Passport](https://github.com/laravel/passport).

Создание ключей шифрования (необходимы для создания токенов безопасного доступа). Кроме того, команда добавит клиентов "
personal access" и "password grant", которые будут использоваться для генерации токенов доступа

```bash
php artisan passport:install
```

Для авторизации через Lighthouse Laravel используется пакет
[Lighthouse GraphQL Passport Auth](https://github.com/joselfonseca/lighthouse-graphql-passport-auth).

Дополнительную информацию по пакетам можно посмотреть в
документациях [Laravel Passport](https://laravel.com/docs/8.x/passport)
и [Lighthouse GraphQL Passport Auth](https://lighthouse-php-auth.com/tutorials/getting-started/).

## Использование Docker Compose profiles

Подробнее в официальной [документации docker-compose](https://docs.docker.com/compose/profiles/)

* Если необходимо использовать mailhog, adminer, то прописываем в .env переменную `COMPOSE_PROFILES=dev-profile` и
  выполняем `make up`
* При отсутствии COMPOSE_PROFILES в .env, можно передавать из командной строки
