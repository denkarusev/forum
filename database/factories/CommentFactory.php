<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'comment' => $this->faker->realText(200),
            'author_id' => User::factory()->create()->id,
            'post_id' => Post::factory()->create()->id,
            'created_at' => $this->faker->dateTimeBetween('-3 month'),
            'updated_at' => $this->faker->dateTimeBetween('-3 month'),
        ];
    }
}
