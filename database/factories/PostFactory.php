<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->realText(120),
            'author_id' => User::factory()->create()->id,
            'body' => $this->faker->realTextBetween(3, 1000),
            'created_at' => $this->faker->dateTimeBetween('-3 month'),
            'updated_at' => $this->faker->dateTimeBetween('-3 month'),
        ];
    }
}
