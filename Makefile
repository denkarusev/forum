SHELL=/bin/bash -e

.DEFAULT_GOAL := help

-include .env

ifeq ($(PROJECT_INTERFACE),)
	PROJECT_INTERFACE := $(shell ./scripts/get-free-interface.sh)
endif
ifeq ($(PROJECT_ID),)
# Абсолютный путь до последнего включенного (include) файла
	ABS_FILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
	PROJECT_ID := $(notdir $(patsubst %/,%,$(dir $(ABS_FILE_PATH))))
endif
ifeq ($(PROJECT_DOMAIN),)
	PROJECT_DOMAIN := "${PROJECT_ID}"
endif

export PROJECT_ID
export PROJECT_DOMAIN
export PROJECT_INTERFACE

help: ## Справка
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

COMPOSE_YML = \
	-f docker-compose.yml \
	-f docker/docker-compose-profiles.yml

ifeq ($(ENVIRONMENT),DEV)
COMPOSE_YML = \
	-f docker-compose.dev.yml
endif

ifeq ($(ENVIRONMENT),PROD)
COMPOSE_YML = \
	-f docker-compose.prod.yml
endif

build: ## Билд проекта
	@docker-compose ${COMPOSE_YML} build --build-arg UID=$$(id -u) --build-arg GID=$$(id -g)

prepare-app: composer-install key-generate migrate passport-keys clear-cache ## Первый запуск
	@echo -e "Make: App is completed. \n"
	@echo -e "\033[31m\033[1mATTENTION:\033[0m Please copy \033[32m\033[1mpassword grant\033[0m \033[33m\033[1mClient ID\033[0m and \033[33m\033[1mClient secret\033[0m to .env  \n"

up: ## Запуск проекта
	@./scripts/ports-free-check.sh
	@docker-compose ${COMPOSE_YML} up -d --remove-orphans

down: ## Остановка проекта
	@docker-compose ${COMPOSE_YML} down

restart: down up

composer-update: ## Обновление composer
	@docker-compose ${COMPOSE_YML} exec --user www-data php composer update -d /var/www/html

composer-install: ## Установка composer
	@docker-compose ${COMPOSE_YML} exec --user www-data php composer install -d /var/www/html

key-generate: ## Генерирование ключа приложения
	@docker-compose exec --user www-data php php artisan key:generate

migrate: ## Запустить миграции
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan migrate

passport-keys: ## Генерирование ключей для passport
	@docker-compose exec --user www-data php php artisan passport:install

create-admin: ## Создание администратора
	@docker-compose exec --user www-data php php artisan orchid:admin

clear-cache:
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan config:clear

env: ## Генерация .env файла проекта
	@test -f .env && echo -ne "\n# ===== Скопировано с .env " >> .env.backup \
		&& date --iso-8601=seconds | tr -d "\n" >> .env.backup \
		&& echo -ne " ====="  >> .env.backup \
		&& cat .env >> .env.backup \
		&& echo "Предыдущая конфигурация сохранена в .env.backup" \
		|| echo
	@echo -e "\n# Домен на котором будет доступен проект" > .env
	@echo "PROJECT_DOMAIN=${PROJECT_DOMAIN}" >> .env
	@echo '# Интерфейс (IP адрес) на котором будут слушать докеры проекта' >> .env
	@echo "PROJECT_INTERFACE=${PROJECT_INTERFACE}" >> .env
	@echo "XDEBUG_REMOTE_HOST=${PROJECT_DOMAIN}" >> .env
	@echo >> .env
	@cat .env.example >> .env
	@echo ".env создан"

install: ## Установка проекта
	@grep '${PROJECT_DOMAIN}' /etc/hosts 1>/dev/null || sudo sh -c "echo '${PROJECT_INTERFACE} ${PROJECT_DOMAIN}' >> /etc/hosts"
	@echo '${PROJECT_DOMAIN} записан в /etc/hosts и будет доступен по адресу http://${PROJECT_DOMAIN}'

db-fresh: ## Пересборка БД + сидеры
	@docker-compose exec --user www-data php php artisan migrate:fresh --seed --force

clear: ## Сброс всех видов кэшей
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan optimize:clear
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan config:clear
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan route:clear
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan event:clear
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan view:clear
	@docker-compose ${COMPOSE_YML} exec --user www-data php php artisan cache:clear

test: clear ## Запуск тестов
	@docker-compose exec --user www-data php php artisan test

INIT_SQL = create database if not exists ${DB_DATABASE_TEST}; grant ALL on ${DB_DATABASE_TEST}.* to ${DB_USERNAME}@'%'; flush privileges;
create-test-db: ## Создание тестовой базы данных
	@docker-compose exec mariadb mysql -uroot -p$(DB_ROOT_PASSWORD) -e "${INIT_SQL}"
	@echo "База данных $(DB_DATABASE_TEST) создана"

bash: ## Запуск консоли
	@docker-compose exec --user www-data php bash

default: help
