<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Main;

use Orchid\Screen\Layouts\Chart;

class PostChartBar extends Chart
{
    /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'График добавления тем';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'bar';

    protected $height = 400;
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'posts';

    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = true;
}
