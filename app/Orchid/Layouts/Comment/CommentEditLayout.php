<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Comment;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class CommentEditLayout extends Rows
{
    /**
     * @var Comment
     */
    protected $comment;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $this->comment = $this->query->get('comment');

        return [
            Label::make('comment.id')
                ->title('ID комментария:')
                ->vertical()
                ->canSee($this->comment->exists),

            $this->checkRequired(
                TextArea::make('comment.comment')
                    ->title('Текст комментария:')
                    ->placeholder('Текст комментария...')
                    ->rows(5)
                    ->max(255)
            ),

            $this->checkRequired(
                Select::make('comment.author_id')
                    ->title('Автор:')
                    ->fromModel(User::class, 'name')
                    ->empty('Не выбрано')
            ),

            $this->checkRequired(
                Select::make('comment.post_id')
                    ->title('ID темы:')
                    ->popover('ID темы, к которой привязываем комментарий')
                    ->fromQuery(Post::orderBy('id', 'ASC'), 'id')
                    ->empty('Не выбрано')
            ),

            Label::make('comment.created_at')
                ->title('Добавлен:')
                ->canSee($this->comment->exists),

            Label::make('comment.updated_at')
                ->title('Изменен:')
                ->canSee($this->comment->exists),
        ];
    }

    /**
     * @param Field $field
     * @return Field
     */
    protected function checkRequired(Field $field): Field
    {
        if (!$this->comment->exists) {
            $field->required();
        }

        return $field;
    }
}
