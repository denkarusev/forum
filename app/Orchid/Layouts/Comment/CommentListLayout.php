<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Comment;

use App\Models\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    protected $target = 'comments';

    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')
                ->width('7%')
                ->sort()
                ->filter(Input::make())
                ->render(function (Comment $comment) {
                    return $comment->id;
                }),

            TD::make('comment', 'Комментарий')
                ->width('45%')
                ->sort()
                ->filter(Input::make())
                ->render(function (Comment $comment) {
                    return Link::make($comment->comment)
                        ->route('platform.comments.edit', $comment);
                }),

            TD::make('post_id', 'ID темы')
                ->popover('ID темы, к которому относится комментарий')
                ->filter(Input::make())
                ->render(function (Comment $comment) {
                    return Link::make((string)$comment->post_id)
                        ->route('platform.posts.edit', $comment->post);
                }),

            TD::make('author', 'Автор')
                ->render(function (Comment $comment) {
                    return $comment->author->name;
                }),

            TD::make('updated_at', 'Изменен')
                ->sort()
                ->render(function (Comment $comment) {
                    return $comment->updated_at->format('d.m.Y h:i:s');
                }),
        ];
    }
}
