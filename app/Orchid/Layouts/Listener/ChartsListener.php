<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Listener;

use App\Orchid\Layouts\Main\CommentChartBar;
use App\Orchid\Layouts\Main\PostChartBar;
use Illuminate\Support\Carbon;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Listener;
use Orchid\Support\Facades\Layout;

class ChartsListener extends Listener
{
    /**
     * List of field names for which values will be listened.
     *
     * @var string[]
     */
    protected $targets = [
        'start_date',
        'end_date'
    ];

    /**
     * What screen method should be called
     * as a source for an asynchronous request.
     *
     * The name of the method must
     * begin with the prefix "async"
     *
     * @var string
     */
    protected $asyncMethod = 'asyncDateCharts';

    /**
     * @return Layout[]
     */
    protected function layouts(): array
    {
        return [
            Layout::block(
                Layout::rows([
                    Group::make([
                        Input::make('start_date')
                            ->type('date')
                            ->value($this->getStartDate())
                            ->title('Начало')
                            ->horizontal(),

                        Input::make('end_date')
                            ->type('date')
                            ->value($this->getEndDate())
                            ->title('Конец')
                            ->horizontal()
                    ]),
                ]),
            )
                ->title('Статистика')
                ->description(
                    'Укажите необходимые периоды для построения графиков<br><b>По умолчанию:</b> выводятся данные за последний месяц'
                ),

            Layout::tabs([
                'График добавления тем' => PostChartBar::class,
                'График добавления комментариев' => CommentChartBar::class,
            ]),
        ];
    }

    private function getStartDate(): string
    {
        return ($this->query->get('start') ?: Carbon::now()->subMonth(1))->format('Y-m-d');
    }

    private function getEndDate(): string
    {
        return ($this->query->get('end') ?: Carbon::now()->subMonth(0))->format('Y-m-d');
    }
}
