<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Post;

use App\Models\Post;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PostListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'posts';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')
                ->width('8%')
                ->sort()
                ->filter(Input::make())
                ->render(function (Post $post) {
                    return $post->id;
                }),

            TD::make('title', 'Заголовок')
                ->width('60%')
                ->sort()
                ->filter(Input::make())
                ->render(function (Post $post) {
                    return Link::make($post->title)
                        ->route('platform.posts.edit', $post);
                }),

            TD::make('author', 'Автор')
                ->render(function (Post $post) {
                    return $post->author->name;
                }),

            TD::make('updated_at', 'Изменен')
                ->sort()
                ->render(function (Post $post) {
                    return $post->updated_at->format('d.m.Y h:i:s');
                }),
        ];
    }
}
