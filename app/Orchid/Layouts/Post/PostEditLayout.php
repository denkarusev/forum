<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Post;

use App\Models\Post;
use App\Models\User;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\SimpleMDE;
use Orchid\Screen\Layouts\Rows;

class PostEditLayout extends Rows
{
    /**
     * @var Post
     */
    protected $post;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $this->post = $this->query->get('post');

        return [
            Label::make('post.id')
                ->title('ID темы:')
                ->vertical()
                ->canSee($this->post->exists),

            $this->checkRequired(
                Input::make('post.title')
                    ->title('Заголовок:')
                    ->placeholder('Заголовок')
                    ->type('text')
                    ->max(255)
            ),

            $this->checkRequired(
                Select::make('post.author_id')
                    ->title('Автор:')
                    ->fromModel(User::class, 'name')
                    ->empty('Не выбрано')
            ),

            $this->checkRequired(
                SimpleMDE::make('post.body')
                    ->title('Описание:')
                    ->placeholder('Описание')
            ),

            Label::make('post.created_at')
                ->title('Создан:')
                ->canSee($this->post->exists),

            Label::make('post.updated_at')
                ->title('Изменен:')
                ->canSee($this->post->exists),
        ];
    }

    /**
     * @param Field $field
     * @return Field
     */
    protected function checkRequired(Field $field): Field
    {
        if (!$this->post->exists) {
            $field->required();
        }

        return $field;
    }
}
