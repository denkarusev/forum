<?php

declare(strict_types=1);

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);
        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            // Главная > Темы
            Menu::make(__('Темы'))
                ->title('Форум')
                ->icon('book-open')
                ->list([
                    Menu::make('Список тем')
                        ->icon('list')
                        ->route('platform.posts'),
                    Menu::make('Добавить тему')
                        ->icon('plus')
                        ->route('platform.posts.create'),
                ]),

            // Главная > Комментарии
            Menu::make(__('Комментарии'))
                ->icon('bubbles')
                ->list([
                    Menu::make('Список комментариев')
                        ->icon('list')
                        ->route('platform.comments'),
                    Menu::make('Добавить комментарий')
                        ->icon('plus')
                        ->route('platform.comments.create'),
                ]),

            // Главная > Профиль
            Menu::make(__('Profile'))
                ->icon('user')
                ->route('platform.profile')
                ->title(__('Users')),

            Menu::make(__('Users'))
                ->icon('people')
                ->route('platform.systems.users')
                ->permission('platform.systems.users'),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles')
                ->title(__('Access rights')),
        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }
}
