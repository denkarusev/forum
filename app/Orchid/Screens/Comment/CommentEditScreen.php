<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Comment;

use App\Models\Comment;
use App\Models\Post;
use App\Orchid\Layouts\Comment\CommentEditLayout;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Toast;

class CommentEditScreen extends Screen
{
    /**
     * Заголовок
     *
     * @var string
     */
    public $name = 'Редактирование комментария';

    /**
     * Описание
     *
     * @var string
     */
    public $description = 'Форма редактирования';

    /**
     * @var Comment
     */
    protected $comment;

    /**
     * @param Comment $comment
     * @return Comment[]
     */
    public function query(Comment $comment): array
    {
        $this->comment = $comment;

        if (!$comment->exists) {
            $this->name = 'Добавление комментария';
            $this->description = 'Форма добавления';
        }

        return [
            'comment' => $comment,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Close')
                ->icon('close')
                ->method('close')
                ->canSee($this->comment->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->comment->exists),

            Button::make('Save')
                ->icon('save-alt')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            CommentEditLayout::class,
        ];
    }

    /**
     * @param Comment $comment
     * @param Request $request
     */
    public function save(Comment $comment, Request $request)
    {
        $request->validate([
            'comment.comment' => ['required', 'max:1000'],
            'comment.author_id' => ['required'],
            'comment.post_id' => ['required'],
        ]);

        $commentRequest = $request->get('comment');
        if ((empty($postId = $commentRequest['post_id'])) || !Post::find($postId)) {
            Alert::error('Ошибка при сохранении комментария: Темы с таким ID не существует.');
            return null;
        }

        $comment->fill($commentRequest);
        $comment->save();

        Toast::info('Комментарий успешно сохранен.');

        return redirect()->route('platform.comments');
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function remove(Comment $comment)
    {
        $comment->delete();

        Toast::info('Комментарий успешно удален.');

        return redirect()->route('platform.comments');
    }

    /**
     * @return RedirectResponse
     */
    public function close()
    {
        return redirect()->route('platform.comments');
    }
}
