<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Comment;

use App\Models\Comment;
use App\Orchid\Layouts\Comment\CommentListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class CommentListScreen extends Screen
{
    /**
     * Заголовок
     *
     * @var string
     */
    public $name = 'Комментарии';

    /**
     * Описание
     *
     * @var string
     */
    public $description = 'Список комментариев';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'comments' => Comment::filters()->paginate(15),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.comments.create'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            CommentListLayout::class
        ];
    }
}
