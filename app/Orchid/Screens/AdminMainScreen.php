<?php

declare(strict_types=1);

namespace App\Orchid\Screens;

use App\Models\Comment;
use App\Models\Post;
use App\Orchid\Layouts\Listener\ChartsListener;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Orchid\Platform\Models\User;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Layout;

class AdminMainScreen extends Screen
{
    /**
     * Заголовок
     *
     * @var string
     */
    public $name = 'Главная страница';

    /**
     * @param Request $request
     * @return array
     */
    public function query(Request $request): array
    {
        $start = Carbon::now()->subMonth(1);
        $end = Carbon::now()->subDay(0);

        return [
            'user' => $request->user(),
            'posts' => [
                Post::countByDays($start, $end)->toChart('posts'),
            ],
            'comments' => [
                Comment::countByDays($start, $end)->toChart('comments'),
            ],
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            ChartsListener::class,

            Layout::legend('user', [
                Sight::make('id'),
                Sight::make('name'),
                Sight::make('email'),
                Sight::make('email_verified_at', 'Email Verified')->render(function (User $user) {
                    return $user->email_verified_at === null
                        ? '<i class="text-danger">●</i> False'
                        : '<i class="text-success">●</i> True';
                }),
                Sight::make(__('Создан'))->render(function (User $user) {
                    return $user->created_at->format('d.m.Y H:i:s');
                }),
                Sight::make(__('Изменен'))->render(function (User $user) {
                    return $user->updated_at->format('d.m.Y H:i:s');
                }),
            ])->title('Текущий пользователь'),
        ];
    }

    /**
     * @param $start_date
     * @param $end_date
     * @return array
     */
    public function asyncDateCharts($start_date, $end_date): array
    {
        $start = $start_date ? Carbon::createFromFormat('Y-m-d', $start_date) : Carbon::now()->subMonth(1);
        $end = $end_date ? Carbon::createFromFormat('Y-m-d', $end_date) : Carbon::now()->subMonth(0);

        return [
            'posts' => [
                Post::countByDays($start, $end)->toChart('posts'),
            ],
            'comments' => [
                Comment::countByDays($start, $end)->toChart('comments'),
            ],
            'start' => $start,
            'end' => $end
        ];
    }
}
