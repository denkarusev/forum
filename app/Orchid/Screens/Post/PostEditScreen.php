<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Post;

use App\Models\Post;
use App\Orchid\Layouts\Post\PostEditLayout;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class PostEditScreen extends Screen
{
    /**
     * Заголовок
     *
     * @var string
     */
    public $name = 'Редактирование темы';

    /**
     * Описание
     *
     * @var string
     */
    public $description = 'Форма редактирования';

    /**
     * @var Post
     */
    protected $post;

    /**
     * @param Post $post
     * @return Post[]
     */
    public function query(Post $post): array
    {
        $this->post = $post;

        if (!$post->exists) {
            $this->name = 'Добавление темы';
            $this->description = 'Форма добавления';
        }

        return [
            'post' => $post,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Close')
                ->icon('close')
                ->method('close')
                ->canSee($this->post->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->post->exists),

            Button::make('Save')
                ->icon('save-alt')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            PostEditLayout::class,
        ];
    }

    /**
     * @param Post $post
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Post $post, Request $request)
    {
        $request->validate([
            'post.title' => ['required', 'unique:posts,title,' . $post->id, 'max:255'],
            'post.body' => ['required', 'min:3', 'max:1000'],
            'post.author_id' => ['required']
        ]);

        $post->fill($request->get('post'));
        $post->save();

        Toast::info('Тема успешно сохранена.');

        return redirect()->route('platform.posts');
    }

    /**
     * @param Post $post
     * @return RedirectResponse
     */
    public function remove(Post $post)
    {
        $post->delete();

        Toast::info('Тема успешно удалена.');

        return redirect()->route('platform.posts');
    }

    /**
     * @return RedirectResponse
     */
    public function close()
    {
        return redirect()->route('platform.posts');
    }
}
