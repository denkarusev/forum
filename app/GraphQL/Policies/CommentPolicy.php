<?php

namespace App\GraphQL\Policies;

use App\Models\Comment;
use App\Models\User;

class CommentPolicy
{
    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function update(User $user, Comment $comment): bool
    {
        return optional($user)->id === $comment->author_id;
    }

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function delete(User $user, Comment $comment): bool
    {
        return optional($user)->id === $comment->author_id;
    }
}
