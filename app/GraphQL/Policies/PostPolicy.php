<?php

namespace App\GraphQL\Policies;

use App\Models\Post;
use App\Models\User;

class PostPolicy
{
    /**
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function update(User $user, Post $post): bool
    {
        return optional($user)->id === $post->author_id;
    }

    /**
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function delete(User $user, Post $post): bool
    {
        return optional($user)->id === $post->author_id;
    }
}
