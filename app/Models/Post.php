<?php

namespace App\Models;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Database\Factories\PostFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Orchid\Filters\Filterable;
use Orchid\Filters\HttpFilter;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property string $title
 * @property int $author_id
 * @property string $body
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $author
 * @property-read Collection|Comment[] $comment
 * @property-read int|null $comment_count
 * @method static Builder|Post newModelQuery()
 * @method static Builder|Post newQuery()
 * @method static Builder|Post query()
 * @method static Builder|Post whereAuthorId($value)
 * @method static Builder|Post whereBody($value)
 * @method static Builder|Post whereCreatedAt($value)
 * @method static Builder|Post whereId($value)
 * @method static Builder|Post whereTitle($value)
 * @method static Builder|Post whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static Builder|Post countByDays($startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 * @method static Builder|Post countForGroup(string $groupColumn)
 * @method static Builder|Post defaultSort(string $column, string $direction = 'asc')
 * @method static PostFactory factory(...$parameters)
 * @method static Builder|Post filters(?HttpFilter $httpFilter = null)
 * @method static Builder|Post filtersApply(array $filters = [])
 * @method static Builder|Post filtersApplySelection($selection)
 * @method static Builder|Post sumByDays(string $value, $startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 * @method static Builder|Post valuesByDays(string $value, $startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 */
class Post extends Model
{
    use HasFactory, AsSource, Filterable, Chartable;

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'title',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'title',
        'updated_at',
    ];

    protected $table = 'posts';
    protected $fillable = ['title', 'body', 'author_id'];

    public function author(){
        return $this->belongsTo(User::class);
    }

    public function comment(){
        return $this->hasMany(Comment::class);
    }
}
