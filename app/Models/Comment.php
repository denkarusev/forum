<?php

namespace App\Models;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Database\Factories\CommentFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Orchid\Filters\Filterable;
use Orchid\Filters\HttpFilter;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Comment
 *
 * @property int $id
 * @property string $comment
 * @property int $author_id
 * @property int $post_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $author
 * @property-read Post $post
 * @method static Builder|Comment newModelQuery()
 * @method static Builder|Comment newQuery()
 * @method static Builder|Comment query()
 * @method static Builder|Comment whereAuthorId($value)
 * @method static Builder|Comment whereComment($value)
 * @method static Builder|Comment whereCreatedAt($value)
 * @method static Builder|Comment whereId($value)
 * @method static Builder|Comment wherePostId($value)
 * @method static Builder|Comment whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static Builder|Comment countByDays($startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 * @method static Builder|Comment countForGroup(string $groupColumn)
 * @method static Builder|Comment defaultSort(string $column, string $direction = 'asc')
 * @method static CommentFactory factory(...$parameters)
 * @method static Builder|Comment filters(?HttpFilter $httpFilter = null)
 * @method static Builder|Comment filtersApply(array $filters = [])
 * @method static Builder|Comment filtersApplySelection($selection)
 * @method static Builder|Comment sumByDays(string $value, $startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 * @method static Builder|Comment valuesByDays(string $value, $startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 */
class Comment extends Model
{
    use HasFactory, AsSource, Filterable, Chartable;

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'comment',
        'post_id',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'comment',
        'post_id',
        'updated_at'
    ];

    protected $table = 'comments';
    protected $fillable = ['comment', 'author_id', 'post_id'];

    public function author(){
        return $this->belongsTo(User::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
