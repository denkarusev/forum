<?php

declare(strict_types=1);

use App\Orchid\Screens\AdminMainScreen;
use App\Orchid\Screens\Comment\CommentEditScreen;
use App\Orchid\Screens\Comment\CommentListScreen;
use App\Orchid\Screens\Post\PostEditScreen;
use App\Orchid\Screens\Post\PostListScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', AdminMainScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('User'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{roles}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Форум > Темы > Редактирование
Route::screen('posts/{posts}/edit', PostEditScreen::class)
    ->name('platform.posts.edit')
    ->breadcrumbs(function (Trail $trail, $posts) {
        return $trail
            ->parent('platform.posts')
            ->push('Редактирование', route('platform.posts.edit', $posts));
    });

// Форум > Темы > Добавление
Route::screen('posts/create', PostEditScreen::class)
    ->name('platform.posts.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.posts')
            ->push('Добавление', route('platform.posts.create'));
    });

// Форум > Темы
Route::screen('posts', PostListScreen::class)
    ->name('platform.posts')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Темы', route('platform.posts'));
    });

// Форум > Комментарии > Редактирование
Route::screen('comments/{comments}/edit', CommentEditScreen::class)
    ->name('platform.comments.edit')
    ->breadcrumbs(function (Trail $trail, $posts) {
        return $trail
            ->parent('platform.comments')
            ->push('Редактирование', route('platform.comments.edit', $posts));
    });

// Форум > Темы > Добавление
Route::screen('comments/create', CommentEditScreen::class)
    ->name('platform.comments.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.comments')
            ->push('Добавление', route('platform.comments.create'));
    });

// Форум > Темы
Route::screen('comments', CommentListScreen::class)
    ->name('platform.comments')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Комментарии', route('platform.comments'));
    });
