<?php

namespace Tests\Feature\Api;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test show method
     */
    public function testShowPost()
    {
        /** @var Post $post */
        $post = Post::factory()->create();

        $this
            ->json('GET', route('posts.show', ['post' => $post->id]))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => $post->title,
                    'body'  => $post->body,
                ]
            ]);

        $this
            ->json('GET', route('posts.show', ['post' => 100]))
            ->assertStatus(404);
    }

    /**
     * Test store method
     */
    public function testStorePost()
    {
        /** @var User $user */
        $user = Passport::actingAs(
            User::factory()->create(),
        );

        /** @var Post $post */
        $post = Post::factory()->make();

        $this
            ->json('POST', route('posts.store'), $post->toArray())
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'title' => $post->title,
                    'body'  => $post->body,
                ]
            ]);
    }

    /**
     * Test update method
     */
    public function testUpdatePost()
    {
        /** @var User $user */
        $user = Passport::actingAs(
            User::factory()->create(),
        );

        /** @var Post $post */
        $post = Post::factory()->create();

        /** @var Post $editedPost */
        $editedPost = Post::factory()->make();

        $this
            ->json('PUT', route('posts.update', ['post' => $post->id]), $editedPost->toArray())
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => $editedPost->title,
                    'body'  => $editedPost->body,
                ]
            ]);
    }

    /**
     * Test destroy method
     */
    public function testDeletePost()
    {
        /** @var User $user */
        $user = Passport::actingAs(
            User::factory()->create(),
        );

        /** @var Post $post */
        $post = Post::factory()->create();

        $this
            ->json('DELETE', route('posts.destroy', ['post' => $post->id]))
            ->assertStatus(204);
    }
}
