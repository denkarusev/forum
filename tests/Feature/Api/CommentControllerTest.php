<?php

namespace Tests\Feature\Api;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test store method
     */
    public function testStoreComment()
    {
        /** @var User $user */
        $user = Passport::actingAs(
            User::factory()->create()
        );

        /** @var Post $post */
        $post = Post::factory()->create();

        /** @var Comment $comment */
        $comment = Comment::factory()->make();

        $this
            ->json('POST', route('posts.comments.store', ['post' => $post->id]), $comment->toArray())
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'comment' => $comment->comment
                ]
            ]);

        $this
            ->json('POST', route('posts.comments.store', ['post' => 99999]), $comment->toArray())
            ->assertStatus(404);
    }

    /**
     * Test show method
     */
    public function testShowComment()
    {
        /** @var Comment $comment */
        $comment = Comment::factory()->create();

        /** @var Post $post */
        $post = Post::factory()->create();
        $post->comment()->save($comment);

        $this
            ->json('GET', route('posts.comments.show', ['post' => $post->id, 'comment' => $comment->id]))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'comment' => $comment->comment
                ]
            ]);

        $this
            ->json('GET', route('posts.comments.show', ['post' => 9999, 'comment' => 9999]))
            ->assertStatus(404);
    }

    /**
     * Test update method
     */
    public function testUpdateComment()
    {
        /** @var User $user */
        $user = Passport::actingAs(
            User::factory()->create()
        );

        /** @var Comment $comment */
        $comment = Comment::factory()->create();

        /** @var Comment $editedComment */
        $editedComment = Comment::factory()->create();

        /** @var Post $post */
        $post = Post::factory()->create();
        $post->comment()->save($comment);

        $this
            ->json('PUT', route('posts.comments.update', ['post' => $post->id, 'comment' => $comment->id]), $editedComment->toArray())
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'comment' => $editedComment->comment
                ]
            ]);

        $this
            ->json('PUT', route('posts.comments.update', ['post' => 9999, 'comment' => 9999]), $editedComment->toArray())
            ->assertStatus(404);

    }

    /**
     * Test delete method
     */
    public function testDestroyComment()
    {
        /** @var User $user */
        $user = Passport::actingAs(
            User::factory()->create()
        );

        /** @var Comment $comment */
        $comment = Comment::factory()->create();

        /** @var Post $post */
        $post = Post::factory()->create();
        $post->comment()->save($comment);

        $this
            ->json('DELETE', route('posts.comments.destroy', ['post' => $post->id, 'comment' => $comment->id]))
            ->assertStatus(204);

        $this
            ->json('DELETE', route('posts.comments.destroy', ['post' => 9999, 'comment' => 9999]))
            ->assertStatus(404);
    }
}
