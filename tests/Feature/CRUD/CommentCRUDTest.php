<?php

declare(strict_types=1);

namespace Tests\Feature\CRUD;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Tests\TestCase;

class CommentCRUDTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use MakesGraphQLRequests;

    public function testCreate(): void
    {
        /** @var Comment $comment */
        $comment = Comment::factory()->create();

        $this->graphQL(
        /** @lang GraphQL */
            '{
            comment(id: ' . $comment->id . '){
                id
            }
        }'
        )->assertJson([
            'data' => [
                'comment' => [
                    'id' => (string)$comment->id
                ]
            ]
        ]);
    }

    public function testUpdate(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Comment $comment */
        $comment = Comment::factory(['author_id' => $user->id])->create();

        /** Fake Auth Token*/
        Passport::actingAs(
            $user,
        );

        $this->graphQL(
        /** @lang GraphQL */
            'mutation {
                updateComment(
                    id: ' . $comment->id . '
                    comment: "' . $this->faker->text(100) . '"
                ) {
                    id
                }
            }',
        )->assertJson([
            'data' => [
                'updateComment' => [
                    'id' => (string)$comment->id
                ]
            ]
        ]);
    }

    public function testDelete()
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Comment $comment */
        $comment = Comment::factory(['author_id' => $user->id])->create();

        /** Fake Auth Token*/
        Passport::actingAs(
            $user,
        );

        $this->graphQL(
        /** @lang GraphQL */
            'mutation {
                deleteComment(
                    id: ' . $comment->id . '
                ) {
                    id
                }
            }',
        )->assertJson([
            'data' => [
                'deleteComment' => [
                    'id' => (string)$comment->id
                ]
            ]
        ]);
    }
}
