<?php

declare(strict_types=1);

namespace Tests\Feature\CRUD;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Tests\TestCase;

class PostCRUDTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use MakesGraphQLRequests;

    public function testCreate(): void
    {
        /** @var Post $post */
        $post = Post::factory()->create();

        $this->graphQL(
        /** @lang GraphQL */
            '{
            post(id: ' . $post->id . '){
                id
            }
        }'
        )->assertJson([
            'data' => [
                'post' => [
                    'id' => (string)$post->id
                ]
            ]
        ]);
    }

    public function testUpdate(): void
    {
        /** @var Post $post */
        $post = Post::factory()->create();

        /** Fake Auth Token*/
        Passport::actingAs(
            $post->author,
        );

        $this->graphQL(
        /** @lang GraphQL */
            'mutation {
                updatePost(
                    id: ' . $post->id . '
                    body: "' . $this->faker->text(100) . '"
                ) {
                    id
                }
            }',
        )->assertJson([
            'data' => [
                'updatePost' => [
                    'id' => (string)$post->id
                ]
            ]
        ]);
    }

    public function testDelete()
    {
        /** @var Post $post */
        $post = Post::factory()->create();

        /** Fake Auth Token*/
        Passport::actingAs(
            $post->author,
        );

        $this->graphQL(
        /** @lang GraphQL */
            'mutation {
                deletePost(
                    id: ' . $post->id . '
                ) {
                    id
                }
            }',
        )->assertJson([
            'data' => [
                'deletePost' => [
                    'id' => (string)$post->id
                ]
            ]
        ]);
    }
}
